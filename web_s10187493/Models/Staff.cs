﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace web_s10187493.Models
{
    public class Staff
    {
        //Found in Lecture 3 slides
        [Display(Name = "ID")]
        public int StaffId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }    

        public char Gender { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Date of Birth")]
        public DateTime DOB { get; set; }

        public string Nationality { get; set; }

        [Display(Name = "Email Address")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-za-z0-9.-]+\.[A-Za-z]{2,4}")]
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists(ErrorMessage = "Email address already exists!")]
        public string Email { get; set; }

        [Display(Name = "Monthly Salary(SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        [Range(1.00, 10000.00)]
        public decimal Salary { get; set; }

        [Display(Name = "Full-Time Staff")]
        public bool IsFullTime { get; set; }

        public int? BranchNo { get; set; }
    }
}
