﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_s10187493.Models
{
    public class BranchViewModel
    {
        public List<Branch> branchList { get; set; }

        public List<Staff> staffList { get; set; }
    }
}
