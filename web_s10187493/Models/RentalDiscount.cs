﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_s10187493.Models
{
    public class RentalDiscount
    {    
        public string Description { get; set; }
        public double DiscountPercent { get; set; }
        public bool Selected { get; set; }
    }
}

