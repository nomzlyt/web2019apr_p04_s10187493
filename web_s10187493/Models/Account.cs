﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_s10187493.Models
{
    public class Account
    {
        public string Message { get; set; }
        public Student Student { get; set; }
    }
    public class Student
    {
        public string Name { get; set; }
    }
}
