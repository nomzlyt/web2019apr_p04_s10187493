﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_s10187493.Models
{
    public class Rental
    {
        [Display(Name = "Loan Date")]
        [DataType(DataType.Date)] //The textbox for entry of LoanDate will be rendered as a HTML5 date picker
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime LoanDate { get; set; }

        [Display(Name = "Number of Books")]
        public int NumBooks { get; set; }

        [Display(Name = "Number of Days")]
        public int NumDays { get; set; }

        [Display(Name = "Rental Rate (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]  //The value for RentalFee will be displayed with 2 decimal places, and punctuated (,) at the thousand mark.
        public double RentalRate { get; set; }

        [Display(Name = "Due Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime DueDate { get; set; }

        [Display(Name = "Rental Fee (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public double RentalFee { get; set; }

        [Display(Name = "Discount Given (%)")]
        [DisplayFormat(DataFormatString = "{0:#0.0}")]
        public double DiscountPercent { get; set; }

        [Display(Name = "Discount Given (%)")]
        public List<RentalDiscount> Discounts { get; set; }

        [Display(Name = "Amount Payable (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]  
        public double AmountPayable { get; set; }
    }
}
