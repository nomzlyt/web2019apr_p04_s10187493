﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_s10187493.Models
{
    public class Book
    {
        [Display(Name = "Book ID")]
        public int Id { get; set; }
        [Display(Name = "ISBN")]
        public string Isbn { get; set; }
        [Display(Name = "Title")]
        public string Title { get; set; }
        [Display(Name = "Author")]
        public string Author { get; set; }
        [Display(Name = "Pages")]
        public int Pages { get; set; }
        [Display(Name = "Quantity Available")]
        public int Qty { get; set; }
        public string Justification { get; set; }
    }

    public class BookReserve
    {
        public BookReserve(int id)
        {
            BookId = id;
        }
        public int BookId { get; set; }
    }
}
